var bodyParser     =        require("body-parser");


const express = require('express')
const app = express()
const port = 3000
//Here we are configuring express to use body-parser as middle-ware.
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Inicializando librerías requeridas para la traducción y el análisis de lenguaje natural
const NaturalLanguageUnderstandingV1 = require('ibm-watson/natural-language-understanding/v1.js');
const LanguageTranslatorV3 = require('ibm-watson/language-translator/v3');

const analizadorLenguajeNatural = new NaturalLanguageUnderstandingV1({
    version: '2018-11-16',
    iam_apikey: 'urt6BBAxutH9vxS8LnhhCg626LdvP7d2LyxxQlW-fCZk',
    url: 'https://gateway.watsonplatform.net/natural-language-understanding/api'
});

const traductorLenguaje = new LanguageTranslatorV3({
    version: '2019-04-02',
    iam_apikey: 'ux0n8CS1J8v_zAj8GjRCdk3I_PKaRpGqtiCR3Ys34L3Y',
    url: 'https://gateway-wdc.watsonplatform.net/language-translator/api',
});

function traduccion_y_analisis (arrayComentarios, traductor,response){
    var resultado = [];

    arrayComentarios.forEach(comentario => {
        const parametrosTraduccion = {
            text: comentario,
            model_id: 'es-en',
        };
        traductor.translate(parametrosTraduccion)
            .then(resultadoTraduccion => {
                //var parametrosAnalisis = {};

                // La variable "parametrosTraduccion.translations" (que es un array) contiene las traducciones de
                // los comentarios que se extrajeron del post a ser analizado, por lo que se retornará un array conteniendo
                // JSONs con la información del análisis de cada comentario
                    
                var parametrosAnalisis = {
                    'text': resultadoTraduccion.translations[0].translation,
                    "features": {
                        "emotion": {},
                        "keywords": {},
                        "sentiment": {},
                        "categories": { 
                            limit : 3
                        }
                    }
                };
                analizadorLenguajeNatural.analyze(parametrosAnalisis)
                    .then(resultadosAnalisisDeLenguajeNatural => {
                        var elementoInsertar = resultadosAnalisisDeLenguajeNatural;
                        elementoInsertar.comentarioEng=parametrosAnalisis.text;
                        elementoInsertar.comentarioEsp=comentario;
                        resultado.push(elementoInsertar);
                        console.log("esp: "+parametrosAnalisis.text);
                        console.log("eng: "+comentario);
                        console.log("\n---------------\n")
                        elementoInsertar = {};
                        if(resultado.length === arrayComentarios.length) {
                            return response.status(201).json(resultado);
                        }
                        //resultado = JSON.stringify(resultadosAnalisisDeLenguajeNatural);
                    })
                    .catch(err => {
                        console.log("Error on nlu");
                        resultado = -1;
                    });
            })
            .catch(err => {
                console.log("error on translation");
                resultado = -2;
            });
    });
};

// NOT WORKING YET
// Servicio para el análisis de comentarios agrupados
app.post('/analisisComentariosEnGrupo',function(request,response){
    
    // Revisión de la existencia de la data de entrada (comentarios)
    if(!request.body) {
        return response.status(400).send({
            success: 'false',
            message: 'not received'
        });
    }

    // Generamos una cadena "comentariosConcatenados" con todos los comentarios
    // recibidos en la request, concantenados por medio de saltos de línea
    let comentariosConcatenados="";
    request.body.forEach(element => {
        comentariosConcatenados = comentariosConcatenados.concat( element["message"] + "\n" );
    });

    // LLamamos a las funciones de traducción y análisis
    traduccion_y_analisis([comentariosConcatenados],traductorLenguaje,response);
});

// WORKING ALREADY
app.post('/analisisComentariosIndividuales',function(request,response){
    
    // Revisión de la existencia de la data de entrada (comentarios)
    if(!request.body) {
        return response.status(400).send({
            success: 'false',
            message: 'not received'
        });
    }

    // Generamos una cadena "comentariosConcatenados" con todos los comentarios
    // recibidos en la request, concantenados por medio de saltos de línea
    let arrayComentarios=[];
    request.body.forEach(element => {
        arrayComentarios.push(element["message"]);
    });

    // LLamamos a las funciones de traducción y análisis
    traduccion_y_analisis(arrayComentarios,traductorLenguaje,response);
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`))