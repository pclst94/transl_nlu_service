/* const LanguageTranslatorV3 = require('ibm-watson/language-translator/v3');

const languageTranslator = new LanguageTranslatorV3({
  version: '2019-04-02',
  iam_apikey: 'ux0n8CS1J8v_zAj8GjRCdk3I_PKaRpGqtiCR3Ys34L3Y',
  url: 'https://gateway-wdc.watsonplatform.net/language-translator/api',
});

const translateParams = {
  text: ["habla 1", "hablando 2"],
  model_id: 'es-en',
};

languageTranslator.translate(translateParams)
  .then(translationResult => {
    console.log(JSON.stringify(translationResult, null, 2));
  })
  .catch(err => {
    console.log('error:', err);
  }); */

var bodyParser     =        require("body-parser");
const express = require('express')
const app = express()
const port = 3000
//Here we are configuring express to use body-parser as middle-ware.
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
var mysql = require('mysql');

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "root",
  database: "watson_project"
});

function buscarComentariosPorPostId(conexion, post_id){

  let query_busqueda = "SELECT contenido, contenido_traducido FROM comentarios  where post_id = '" + post_id + "'";

  // Primero se verifica si existen comentarios que ya estén almacenados con el post_id que se está buscando
  return new Promise(( resolve, reject ) => {
    conexion.query(query_busqueda, ( err, resultado ) => {
      if (err) return reject(err);
      if (resultado.length > 0){
        // hay comentarios en la bd que están ligados al post_id del post con el que se está trabajando
        resolve(resultado);
      }else {
        //no hay comentarios en la bd que estén ligados al post_id
        resolve(-1);
      }
    });
  });
};

function almacenarComentarios(conexion, parametrosInsercion){
  let query_almacenar = "INSERT INTO comentarios (post_id, contenido, contenido_traducido, json_indicadores) VALUES"+
    "('" + parametrosInsercion.post_id + "', " +
    "'" + parametrosInsercion.comentario + "', " +
    "'" + parametrosInsercion.traduccion + "', " +
    "'" + JSON.stringify(parametrosInsercion.indicadores) + "')";
  return new Promise(( resolve, reject ) => {
    conexion.query(query_almacenar, ( err, resultado ) => {
      if (err) return reject(err);
      resolve(resultado);
    });
  });
}

app.get('/lol',function(request,response){
    
  // Revisión de la existencia de la data de entrada (comentarios)
  if(!request.body && !request.api-key) {
    return response.status(400).send({
      success: 'false',
      message: 'No se ha enviado data o no se ha enviado el api-key'
    });
  }

  con.connect(function(err) {
    if (err) throw err;
    buscarComentariosPorPostId(con, "4121313131")
      .then(resultado => {
        if(resultado !== -1){
          console.log("no");
          return response.status(201).json(resultado);
        }
        else{
          var parms = {
            post_id : request.body.post_id,
            comentario : request.body.comentario,
            traduccion : request.body.traduccion,
            indicadores : request.body.indicadores
          };
          console.log("yes");
          almacenarComentarios(con, parms)
            .then(result => {
              console.log(result)
              /* return response.status(201).json({
                message: "Se realizará el proceso completo"
              }) */
            }).catch(err => {
              console.log("Error on storing: " + err);
              response.status(400).send({
                  success: 'false',
                  message: err
              });
            });
        }
      });
  });
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`))