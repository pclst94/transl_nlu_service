// Inicializando librerías requeridas para la traducción y el análisis de lenguaje natural
const NaturalLanguageUnderstandingV1 = require('ibm-watson/natural-language-understanding/v1.js');
const LanguageTranslatorV3 = require('ibm-watson/language-translator/v3');

const analizadorLenguajeNatural = new NaturalLanguageUnderstandingV1({
    version: '2018-11-16',
    iam_apikey: 'urt6BBAxutH9vxS8LnhhCg626LdvP7d2LyxxQlW-fCZk',
    url: 'https://gateway.watsonplatform.net/natural-language-understanding/api'
});

const traductorLenguaje = new LanguageTranslatorV3({
    version: '2019-04-02',
    iam_apikey: 'YJK7xifw0nAqaz5P6vCN8-2DTLgwMhEiYUDz3V4HaHK-',
    url: 'https://gateway.watsonplatform.net/language-translator/api',
});

// Instanciando un objeto Body parser para poder manejar la data que se recibirá en cada request
var bodyParser     =        require("body-parser");
const express = require('express');
const app = express();
const port = 3000;
// Configurando express para poder usar "body-parser" como middle-ware.
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Inicializando la librería mysql para la conexión con la BD
var mysql = require('mysql');

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "root",
  database: "watson_project",
  charset: 'utf8mb4'
});
con.connect(function(err) {
    if (err) throw err;
    return;
});

// Función para buscar comentarios, que estén ligados a un postId (que viene adjuntado en la request), en la bd
function buscarComentariosPorPostId(conexion, post_id, tipo_comentario){

    let query_busqueda = "SELECT contenido, contenido_traducido, json_indicadores FROM comentarios  where " +
        "post_id = '" + post_id + "' and " +
        "tipo_comentario = '" + tipo_comentario + "'";
    console.log("qry: "+query_busqueda);
    return new Promise(( resolve, reject ) => {
        conexion.query(query_busqueda, ( err, resultado ) => {
            if (err) return reject(err);
            if (resultado.length > 0){
                // hay comentarios en la bd que están ligados al post_id del post con el que se está trabajando
                //conexion.end();
                resolve(resultado);
            }else {
                //no hay comentarios en la bd que estén ligados al post_id
                resolve(-1);
            }
        });
    });
};

// Función para almacenar comentarios y sus respectivos puntajes en la BD para no reutilizar los servicios de Watson
// en caso se quiera analizar un post ya analizado anteriormente, solo usar los datos almacenados en la BD de dicho post
function almacenarComentarios(conexion, parametrosInsercion){
    // Dado que hay dos tipos de análisis (individual y grupal) se decidió agregar la columna "tipo_comentario"
    // debido a que si se quería retribuir la data de un post que fué analizado anteriormente, éste pudo haber sido
    // almacenado mediante el análisis grupal o individual, pudiendo ocasionar conflictos al momento de la retribución
    let query_almacenar = 'INSERT INTO comentarios' +
        '(post_id, contenido, contenido_traducido, json_indicadores, tipo_comentario) ' +
        'VALUES (?, ?, ?, ?, ?)';

    return new Promise(( resolve, reject ) => {
        // Se hace uso del tipo de dato Promise para poder tratar la asincronía de los procesos de consultas a la BD
        conexion.query(query_almacenar, [parametrosInsercion.post_id, parametrosInsercion.comentario, parametrosInsercion.traduccion, JSON.stringify(parametrosInsercion.indicadores), parametrosInsercion.tipo_comentario], ( err, resultado ) => {
            if (err) return reject(err);
            resolve(resultado);
        });
  });
}

function traduccion_y_analisis (post_id, arrayComentariosOriginales, traductor,response, tipo_coment){
    // Inicializamos una variable de tipo array resultadoFinal[] cuyos elementos tendrán la forma:
    /*
    >Ejemplo elemento:
    {
        categories: [{score: 0.699586, label: "/science/mathematics/algebra"},…]
        comentarioEng: "I'm writing to you and it's not responding."
        comentarioEsp: "Le estoy escribiendo y no responde"
        emotion: {document: {emotion: {sadness: 0.068875, joy: 0.50546, fear: 0.121814, disgust: 0.051381, anger: 0.136031}}}
        keywords: []
        language: "en"
        sentiment: {document: {score: -0.961157, label: "negative"}}
        usage: {text_units: 1, text_characters: 43, features: 4}
    }
    */
    var resultadoFinal = [];

    // Se procede a iterar en arrayComentariosOriginales[] para poder traducir y analizar (la traducción de) cada comentario
    arrayComentariosOriginales.forEach(comentarioOriginal => {
        const parametrosTraduccion = {
            text: comentarioOriginal,
            model_id: 'es-en',
        };
        // Se realiza el proceso de traducción llamando a la función "translate" del objeto "traductor", 
        // el cual es una instancia de LanguageTranslatorV3 (de IBM Watson)
        traductor.translate(parametrosTraduccion)
            .then(resultadoTraduccion => {
                // El objeto "parametrosTraduccion.translations[0].translation" contiene la traducción a inglés
                // del comentario analizado y se usará como parámetro en el análisis NLU
                var parametrosAnalisisComentario = {
                    'text': resultadoTraduccion.translations[0].translation,
                    "features": {
                        "emotion": {},
                        "keywords": {},
                        "sentiment": {},
                        "categories": {
                            limit : 3
                        }
                    },
                    "language": "en"
                };

                // Se procede al análisis del comentario traducido a ingles con la función "analyze" del objeto 
                // "analizadorLenguajeNatural", el cual es una instancia de NaturalLanguageUnderstandingV1 (de IBM Watson) 
                analizadorLenguajeNatural.analyze(parametrosAnalisisComentario)
                    .then(resultadosAnalisisDeComentarioIngles => {
                        var elementoInsertar = resultadosAnalisisDeComentarioIngles;
                        elementoInsertar.comentarioEng = resultadoTraduccion.translations[0].translation;
                        elementoInsertar.comentarioEsp = comentarioOriginal;
                        resultadoFinal.push(elementoInsertar);
                        elementoInsertar = {};

                        // Parte para almacenar comentarios en la BD
                        var parms = {
                            post_id : post_id,
                            comentario : comentarioOriginal,
                            traduccion : resultadoTraduccion.translations[0].translation,
                            indicadores : resultadosAnalisisDeComentarioIngles,
                            tipo_comentario : tipo_coment
                        };
                        almacenarComentarios(con, parms).then(resultadoInsert => {
                            console.log("insertao: " + resultadoFinal.length);
                            if(resultadoFinal.length === arrayComentariosOriginales.length) {
                                return response.status(201).json(resultadoFinal);
                            }
                        });
                    })
                    .catch(err => {
                        console.warn("Error nlu de comentario en ingles:\n",err);
                        resultadoFinal = -1;
                    });
            })
            .catch(err => {
                console.warn("Error al momento de traducir: " + comentarioOriginal + "\n",err);
                resultadoFinal = -2;
            });
    });
};

// WORKING ALREADY
app.post('/analisisComentarios',function(request,response){
    // Revisión de la existencia de la data de entrada (comentarios)
    if(!request.body && !request.body.apiKey && !request.body.postId && !request.body.tipoAnalisis) {
        return response.status(400).send({
            success: 'false',
            message: 'Error en la petición HTTP, los datos enviados son incorrectos o inexistentes.'
        });
    }

    let tipoAnalisis = request.body.tipoAnalisis  === "individual" ? 1:2;
    
    buscarComentariosPorPostId(con, request.body.postId, tipoAnalisis)
        .then(resultadoBusqueda => {
            if(resultadoBusqueda !== -1){
                var respuesta = [];
                console.log("Existen comentarios en la bd ligados al post");
                resultadoBusqueda.forEach(elemento => {
                    var elementoInsertar = JSON.parse(elemento.json_indicadores);
                    elementoInsertar.comentarioEsp = elemento.contenido;
                    elementoInsertar.comentarioEng = elemento.contenido_traducido;
                    respuesta.push(elementoInsertar);
                    elementoInsertar = {};
                    if(resultadoBusqueda.length === respuesta.length){
                        console.log("ya hay y bien");
                        return response.status(201).json(respuesta);
                    }
                });
            }
            else{
                console.log("No hay comentarios almacenados en la bd ligados a ese post");
                let arrayComentarios=[];
                if(request.body.tipoAnalisis === "individual"){
                    // CASO ANALISIS INDIVIDUAL:
                    // Se inserta cada uno de los comentarios recibidos de la request a arrayComentarios[]
                    request.body.data.forEach(element => {
                        arrayComentarios.push(element["message"]);
                    });
                }
                else{
                    // CASO ANALISIS GRUPAL:
                    // Generamos una cadena "comentariosConcatenados" con todos los comentarios
                    // recibidos de la request concantenados por medio de saltos de línea,
                    // para insertarlo como único elemento de arrayComentarios[]
                    let comentariosConcatenados="";
                    request.body.data.forEach(element => {
                        comentariosConcatenados = comentariosConcatenados.concat( element["message"] + "\n" );
                    });
                    arrayComentarios.push(comentariosConcatenados);
                    console.log("txt length 2 an: "+arrayComentarios[0].length);
                }

                // Procedemos a realizar el proceso de traducción y análisis (NLU) en forma consecuente
                traduccion_y_analisis(request.body.postId, arrayComentarios, traductorLenguaje, response, tipoAnalisis);
            }
        })
        .catch(err => {
            console.warn("Error al buscar comentarios:\n",err);
            return response.status(500).send({
                success: 'false',
                message: 'Error en la búsqueda de comentarios en la BD.'
            });
        });;
   
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`))